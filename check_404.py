import time
import requests
import settings
def check_404(link):
	requests.packages.urllib3.disable_warnings()
	try:
		print('Checking ' + link)
		req = requests.head(link, allow_redirects=True, verify=False)
		if req.status_code == 200:
			return False
		else:
			return True
	except KeyboardInterrupt:
		print('Ctrl + C  Exiting...')
		exit(0)
	except:
		print('Something wrong, sleep in 10 seconds...')
		time.sleep(10)
def block_by_link(link, cur):
	try:
		cur.execute('UPDATE `webms` SET `blocked` = 1 WHERE link = "{}"'.format(link))
		settings.conn.commit()
	except KeyError:
		pass
def main():
	all_count = 0
	error_count = 0
	links = []
	cur = settings.conn.cursor()
	cur.execute('SELECT `link` FROM `webms` WHERE `link` LIKE "%mp4" AND `blocked` = 0')
	for row in cur:
		links.append(row[0])
	for link in links:
		try:
			if check_404(link):
				print('404 FOUND!!!\nBlocking...')
				block_by_link(link, cur)
				error_count += 1
		except KeyboardInterrupt:
			print('Ctrl + C  Exiting...')
			exit(0)				
		except KeyboardInterrupt:
			print('Ctrl + C  Exiting...')
			exit(0)
		finally:
			all_count += 1
			print('{}/{}'.format(error_count, all_count))		

if __name__ == '__main__':
	main()
#https://2ch.hk//b/src/158086000/15014212986482.mp4

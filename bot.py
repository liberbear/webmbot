#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import sys
import logging
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
import requests
import pymysql
import settings
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)
def get_db_con():
  return pymysql.connect(host=settings.DB_HOST, port=3306, user=settings.DB_USER, passwd=settings.DB_PASSWORD, db=settings.DB_NAME)
def get_stats(con):
    try:
        cur = con.cursor()
        cur.execute("SELECT (SElECT COUNT(DISTINCT(`md5`)) FROM `webms`) as total, (SELECT COUNT(DISTINCT(`md5`)) FROM `webms` WHERE `link` LIKE \"%.gif\" AND `tele_id` <> '') AS gif, (SELECT COUNT(DISTINCT(`md5`)) as webm FROM `webms` WHERE `link` LIKE \"%.mp4\" AND `tele_id` <> '') AS webm")
        return cur.fetchone()
    except:
        pass
    finally:
        cur.close
      
def get_random_gif(con):
    try:
        cur = con.cursor()
        sql = "SELECT `tele_id` FROM `webms` WHERE `link` LIKE \"%gif\" AND `blocked` = 0 AND `tele_id` IS NOT NULL AND `tele_id` <> '' ORDER BY rand() LIMIT 1"
        cur.execute(sql)
        return cur.fetchone()[0]
    except:
        print('Error with getting random gif')
        pass
    finally:
        cur.close
def get_random_video(con):
    try:
        cur = con.cursor()
        sql = "SELECT `tele_id` FROM `webms` WHERE `link` LIKE \"%mp4\" AND `blocked` = 0 AND `tele_id` IS NOT NULL AND `tele_id` <> '' AND `board` = \"b\" ORDER BY rand() LIMIT 1"
        cur.execute(sql)
        return cur.fetchone()[0]
    except:
        print('Error with getting random video')
        pass
    finally:
        cur.close
def start(bot, update):
    keyboard = [[InlineKeyboardButton(text='Проголосовать за бота :)', url='https://telegram.me/storebot?start=webmsbot')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(text='Суп анон\n\nЭтот бот отправит тебе рандомную вебм или гифку\nстоит только написать /webm или /gif \n\n Если понравится проголосуй за меня тут\n', reply_markup=reply_markup)


def help(bot, update):
    keyboard = [[InlineKeyboardButton(text='🦄 Поставить оценку боту 🦄', url='https://telegram.me/storebot?start=webmsbot')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(text='Используй команды /webm или /gif\n\nСпасибо:\n2ch.hk - за контент\n@konfaodnogoantona - за моральное угнетение\n\nПо всем вопросам @liberbear',

                              reply_markup=reply_markup)
def stats(bot, update):
    stats = get_stats(get_db_con())
    update.message.reply_text('Всего в базе {}\nВсего в кеше {}\n\nGif {}\nWebM {}\n\nКогда запилю конвертер mp4 -> webm, их станет больше :)'.format(stats[0], int(stats[1])+int(stats[2]), stats[1], stats[2]))
    
def gif(bot, update):
    link = get_random_gif(get_db_con())
    update.message.reply_document(link, reply_markup=get_two_inlines())
def webm(bot, update):
    link = get_random_video(get_db_con())
    update.message.reply_video(link, reply_markup=get_two_inlines())
def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))
def restart(bot, update):
    bot.send_message(update.message.chat_id, "BOT RESTARTED")
    time.sleep(0.2)
    os.execl(sys.executable, sys.executable, *sys.argv)

#Что-то новенькое сученьки
def get_two_inlines():
    keys = [[InlineKeyboardButton('📺 WebM', callback_data='1'),InlineKeyboardButton
                                                  ('📷 Gif', callback_data='2')]]
    return InlineKeyboardMarkup(inline_keyboard=keys)
def button(bot, update):
    query = update.callback_query
    username = update.effective_user.username
    chat_id = query.message.chat.id
    message_id = query.message.message_id
    #Ответ про оплату
    if int(query.data) == 1:
      bot.send_video(video=get_random_video(get_db_con()),
                          chat_id=chat_id,
                          message_id=message_id,
                          reply_markup=get_two_inlines())
    elif int(query.data) == 2:
      bot.send_document(document=get_random_gif(get_db_con()),
                          chat_id=chat_id,
                          message_id=message_id,
                          reply_markup=get_two_inlines())
def text_caught(bot, update):
  pass
def main():
    print('BOT STARTED')
    updater = Updater(settings.TOKEN)
    dp = updater.dispatcher
    dp.add_handler(CallbackQueryHandler(button))
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("webm", webm))
    dp.add_handler(CommandHandler("gif", gif))
    dp.add_handler(CommandHandler("stats", stats))
    dp.add_handler(CommandHandler("r", restart))
    dp.add_handler(MessageHandler(Filters.text, text_caught))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()

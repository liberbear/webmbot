import json
import requests
import time
import pymysql.cursors
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
def req_page(link):
    r = requests.get(link, verify=False, allow_redirects=True)
    page = r.text
    return page

def get_posts(page):
    try:
        js = json.loads(page)
        posts = js['threads'][0]['posts']
    except ValueError:
        posts = False
    return posts

def get_all_info(posts, board):
    i = 0
    files = {}
    files['names'] = {0: str}
    files['link'] = {0: str}
    files['date_db'] = {0: str}
    files['thumbnail'] = {0: str}
    files['md5'] = {0: str}
    files['nsfw'] = {0: int}
    files['date_2ch'] = {0: int}
    files['board'] = {0: str}
    for post in posts:
        if 'files' in post:
            try:
                for attach in post['files']:
                    files['names'][i] = str(attach['fullname'])
                    files['link'][i] = str('https://2ch.hk/' + attach['path'])
                    files['date_db'][i] = int(time.time())
                    files['thumbnail'][i] = str('https://2ch.hk/' + attach['thumbnail'])
                    files['md5'][i] = str(attach['md5'])
                    files['nsfw'][i] = int(attach['nsfw'])
                    files['date_2ch'][i] = int(post['timestamp'])
                    files['board'][i] =str(board)
                    i += 1
            except KeyError:
                pass
    return files

def find_threads(board):
    thread_nums = []
    first_page = req_page('https://2ch.hk/'+board+'/1.json')
    js_first_page = json.loads(first_page)
    pages_count = js_first_page['pages']
    pages_count.remove(1)
    pages_count.remove(2)
    c = 1
    for page in pages_count:
        a = 0
        board_text = req_page('https://2ch.hk/'+board+'/'+str(c)+'.json')
        board_js = json.loads(board_text)
        for tr in board_js['threads']:
            thread_nums.append(board_js['threads'][a]['thread_num'])
            a += 1
        c += 1
    print('Founded ', len(thread_nums), 'threads in ', board)
    return thread_nums
def write_mysql_db(host, user, password, db, db_info):
    con = pymysql.connect(host=host,
        user=user,
        password=password,
        db=db,
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)
    try:
        with con.cursor() as cur:
            for i in range(len(db_info['names'])):
                if (db_info['link'][i][-4:] == 'webm') or (db_info['link'][i][-3:] in ['gif', 'mp4']):
                    sql = "INSERT INTO `webms` (`names`, `link`, `date_db`, `thumbnail`, `md5`, `nsfw`, `date_2ch`, `board`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                    cur.execute(sql, (db_info['names'][i], db_info['link'][i], db_info['date_db'][i], db_info['thumbnail'][i], db_info['md5'][i], db_info['nsfw'][i], db_info['date_2ch'][i], db_info['board'][i]))
    except pymysql.err.IntegrityError:
        pass
    except:
        print("Database Error")
#        break
        con.close()
        exit(0)
    finally:
        con.commit()
        con.close()
def main():

    '''SETTINGS'''
    boards = ['b', 'a']
    db_file_name = 'webm.db'
    '''SETTINGS END'''
    for board in boards:
        count = 1
        threads = find_threads(board)
        print('Parsing and writing to DB')
        try:
            for num in threads:
                print ('[', board, '] ', count, '/', len(threads))
                posts = get_posts(req_page('https://2ch.hk/'+board+'/res/'+num+'.json'))
                if posts:
                    files = get_all_info(posts, board)
                    write_mysql_db('localhost', 'webmdb', 'I2H5jzOT7Ys19b8Q', 'webmdb', files)
                else:
                    print('Something wrong with posts parsing, skipping that ...')
                count += 1
        except KeyboardInterrupt:
            print('Stopped by user')
            break
            exit(0)
if __name__ == '__main__':
    main()

